﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FrogGame
{
    public class Game
    {
        private Map _map;

        public List<Location> ShortestWay = new List<Location>();
        

        public Game(Map map)
        {
            _map = map;
        }

        public void Play()
        {
            var limit = _map.Finish.Sector - _map.Start.Sector;
            if (limit < 0)
            {
                _map.Finish.Sector += Map.SECTORS;
                limit *= (-1);
            }
            var result = FindWay(new Segment { SelfLocation = _map.Start }, 0, limit);

            if (result - 1 > 0)
                Console.WriteLine($"Found path with {result - 1} steps");
            else
                Console.WriteLine("Not found");

        }

        private int FindWay(Segment node, int depth, int limit)
        {
            

            if (node.SelfLocation.Sector <= _map.Start.Sector + limit)
            {
                if (node.SelfLocation == _map.Finish)
                {
                    return 1;
                }                    

                var availableMoves = new List<Location>
                {
                    node.MiddleMove, node.SecondMoveRight, node.FirstMoveRight, node.SecondMoveLeft, node.FirstMoveLeft
                };
                foreach (var move in availableMoves)
                {   
                    if (move.Segment <= Map.SEGMENTS && move.Segment >= 1  && !_map.Trees.Contains(move))
                    {
                        var result = FindWay(new Segment { SelfLocation = move }, depth + 1, limit);
                        if (result > 0)
                        {
                            ShortestWay.Add(move);
                            return result + 1;
                        }
                    }
                    
                }
            }
            return 0;
        }
    }
}
