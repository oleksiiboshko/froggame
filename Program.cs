﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace FrogGame
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("Please input start location of frog in format [Sector Segment]");
            var startInput = ValidateLocation(Console.ReadLine().Split(" "));
            Console.WriteLine();

            Console.WriteLine("Please input finish location of frog in format [Sector Segment]");
            var finishInput = ValidateLocation(Console.ReadLine().Split(" "));
            Console.WriteLine();

            Console.WriteLine("Please input number of trees on map in format [number]");
            int treesCount;
            if (!int.TryParse(Console.ReadLine(), out treesCount))
                Console.WriteLine("Wrong number! Please restart application!");
            Console.WriteLine();

            Console.WriteLine("Please input location for trees format [Sector Segment]");
            var trees = new List<Location>();
            for (int tree = 0; tree < treesCount; tree++)
            {
                Console.WriteLine($"Please input location for tree [{tree + 1}/ {treesCount}] in format [Sector Segment]");
                var location = ValidateLocation(Console.ReadLine().Split(" "));
                Console.WriteLine();
                trees.Add(new Location { Sector = Convert.ToInt32(location[0]), Segment = Convert.ToInt32(location[1]) });  
            }

            var start = new Location { Sector = Convert.ToInt32(startInput[0]), Segment = Convert.ToInt32(startInput[1]) };
            var finish = new Location { Sector = Convert.ToInt32(finishInput[0]), Segment = Convert.ToInt32(finishInput[1]) };

            if (start == finish)
            {
                Console.WriteLine("Start location cannot be equal to finish location!");
                return;
            }


            var map = new Map
            {
                Start = start,
                Finish = finish,
                Trees = trees
            };
            var game = new Game(map);

            game.Play();

            foreach (var node in game.ShortestWay.OrderBy(n=>n.Sector))
            {
                Console.WriteLine($"Sector: {(node.Sector > Map.SECTORS ? (node.Sector - Map.SECTORS) : node.Sector)} | Segment: {node.Segment}");
            }
        }


        private static string[] ValidateLocation(string[] location)
        {
            int firstInt, secondInt;
            if (location.Length == 2 && int.TryParse(location[0], out firstInt) && int.TryParse(location[1], out secondInt))
                return location;
            else
                Console.WriteLine("Wrong input! Please restart application!");
            return null;
        }

    }
}
