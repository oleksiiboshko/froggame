﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FrogGame
{
    public struct Segment
    {
        public Location SelfLocation { get; set; }

        public Location MiddleMove { get { return new Location { Sector = SelfLocation.Sector + 3, Segment = SelfLocation.Segment }; } }
        public Location FirstMoveLeft { get { return new Location { Sector = SelfLocation.Sector + 1, Segment = SelfLocation.Segment + 2 }; } }
        public Location FirstMoveRight { get { return new Location { Sector = SelfLocation.Sector + 1, Segment = SelfLocation.Segment - 2 }; } }
        public Location SecondMoveLeft { get { return new Location { Sector = SelfLocation.Sector + 2, Segment = SelfLocation.Segment + 1 }; } }
        public Location SecondMoveRight { get { return new Location { Sector = SelfLocation.Sector + 2, Segment = SelfLocation.Segment - 1 }; } }
    }
}
