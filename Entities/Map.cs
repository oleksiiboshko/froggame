﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FrogGame
{
    public class Map
    {
        public Location Start;

        public Location Finish;

        public List<Location> Trees { get; set; }

        public const int SECTORS = 16;

        public const int SEGMENTS = 10;

    }
}
