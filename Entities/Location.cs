﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FrogGame
{
    public struct Location
    {
        public int Sector;
        public int Segment;


        

        public static bool operator ==(Location l1, Location l2)
        {
            return l1.Segment == l2.Segment && l1.Sector == l2.Sector;
        }

        public static bool operator !=(Location l1, Location l2)
        {
            return l1.Segment != l2.Segment | l1.Sector != l2.Sector;
        }
    }
}
